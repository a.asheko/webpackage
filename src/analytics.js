import * as $ from  'jquery'

function creatAnalytics() {
    let counter = 0
    let destroyd = false

    console.log('test')

    const listener = () => counter++

    document.addEventListener('click', listener)

    return {
        destroy() {
            $(document).off('click', listener)
            destroyd = true
        },

        getClicks() {
            if (destroyd) {
                return 'Analytics is destroyed. Total clicks.'
            }
            return counter
        }
    }
}

window.analytics = creatAnalytics()
