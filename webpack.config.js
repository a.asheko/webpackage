const path = require('path')
const HTMLWebpackPlugin = require('html-webpack-plugin')
const {CleanWebpackPlugin} = require('clean-webpack-plugin')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')

const isDev = process.env.NODE_ENV === 'development'
const isProd =!isDev
module.exports={
    context: path.resolve(__dirname, 'src'),
    mode: 'development',
    entry: {
        main:'./index.js',
        analytics:'./analytics.js'
    },
    output: {
        filename: '[name].[contenthash].js',
        path: path.resolve(__dirname, 'dist')
    },
    resolve: {
        extensions: ['.js'],
        alias: {
            '@models':path.resolve(__dirname,'src/models'),
            '@':path.resolve(__dirname,'src')
        }
    },
    optimization: {
        splitChunks: {
            chunks: 'all'
        }
    },

    devServer: {
        port:4200,
        open:true,
        hot:isDev

    },
    plugins: [
        new HTMLWebpackPlugin({
            template: "./index.html",
            minify: {
                collapseWhitespace:isProd
            }
        }),
        new CleanWebpackPlugin(),
        new MiniCssExtractPlugin({
            filename:'[name].[contenthash].css'
        })
    ],

    module: {
        rules: [
            {
                test: /\.css$/,
                use:[{
                    loader:MiniCssExtractPlugin.loader,
                    options: {
                        hmr:isDev,
                        reloadAll:true
                    },
                    },'css-loader'
                ]
            },
            {
                test: /\.(png|jpg|svg|gif)$/,
                use:['file-loader']
            },
            {
                test: /\.(ttf|woff|woff2|eot)$/,
                use:['file-loader']
            }
        ]
    }

}
